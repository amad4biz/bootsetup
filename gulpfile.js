var gulp = require('gulp'),
    sass = require('gulp-sass'),
   uglify= require('gulp-uglify'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCss = require('gulp-clean-css')
    rename = require('gulp-rename'),
    concat = require('gulp-concat');




    var config = {

       bootstrapDir : './sass/bootstrap4',   // node_modules/bootstrap
       public : './public'  //or the directory you want to put you css files

   }




   gulp.task('sass', () =>{
    return gulp.src('./sass/*style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({browsers: ['last 3 versions'],
            cascade: false}))

    .pipe(gulp.dest(config.public + '/css'));

});



gulp.task('minify-css', ()=>{

    return gulp.src('./public/css/style.css')

     .pipe(cleanCss({ debug: true}, function(details){

         console.log(details.name + ': ' + details.stats.originalSize);
         console.log(details.name + ': ' + details.stats.minifiedSize);

    })).pipe(rename({
            suffix: '.min'
        }))
    .pipe(gulp.dest(config.public + '/css'))

});



// javascipts tasks

gulp.task('js', function(){

    return gulp.src([
      './vendor/modernizr-custom.js',
       './vendor//jquery.js',
      './vendor/jquery-ui.min.js',
       config.bootstrapDir + '/js/src/*.js',
       './vendor/jspdf.min.js',
       //add your custom script here
    ]).pipe(concat('allscripts.js'))
    //.pipe(rename('customscripts.min.js'))
    // .pipe(uglify())
    .pipe(gulp.dest(config.public + '/js'))

    //.pipe(gulp.dest(config.public + '/js'))

});


// watch the files to be changed
gulp.task('watch', ['sass', 'minify-css', 'js' ], function(){

    gulp.watch('./sass/*.scss', ['sass']);
     gulp.watch(config.public + '/css/*style.css', ['minify-css']);
     gulp.watch(config.public + '/js/.*js', ['js']);

});


//default task
gulp.task('default',  ['sass', 'js', 'watch'], );
